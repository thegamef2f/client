(function() {
    let selectedCard = null;
    let playerId = null;
    let active = false;
    let playerCards = [];

    function updateStatus(status) {
        active = status.active;
        $('#opponent-name').html(status.opponentName);
        $('#player-name').html(status.playerName);
        if(active && !$('#player-name').hasClass('active')) $('#player-name').addClass('active');
        else if(!active && $('#player-name').hasClass('active')) $('#player-name').removeClass('active');
        if(!active && !$('#opponent-name').hasClass('active')) $('#opponent-name').addClass('active');
        else if(active && $('#opponent-name').hasClass('active')) $('#opponent-name').removeClass('active');

        let cards = $('#opponent .hand-cards');
        cards.html("");
        for (let i = 0; i < status.numOpponentCards; ++i) {
            cards.append('<div class="card"><img height="75px" src="assets/card_gold.svg" /></div>');
        }
        if (status.pilesOpponent.down != 60)
            $('#opponent .pile-down').html(`<img height="75px" src="assets/card_gold_${status.pilesOpponent.down}.svg" />`);
        else
            $('#opponent .pile-down').html(`<img height="75px" src="assets/card_gold.svg" />`);
        if (status.pilesOpponent.up != 1)
            $('#opponent .pile-up').html(`<img height="75px" src="assets/card_gold_${status.pilesOpponent.up}.svg" />`);
        else
            $('#opponent .pile-down').html(`<img height="75px" src="assets/card_gold.svg" />`);
        if (status.pilesPlayer.down != 60)
            $('#player .pile-down').html(`<img height="75px" src="assets/card_gold_${status.pilesPlayer.down}.svg" />`);
        else
            $('#opponent .pile-down').html(`<img height="75px" src="assets/card_gold.svg" />`);
        if (status.pilesPlayer.up != 1)
            $('#player .pile-up').html(`<img height="75px" src="assets/card_gold_${status.pilesPlayer.up}.svg" />`);
        else
            $('#opponent .pile-down').html(`<img height="75px" src="assets/card_gold.svg" />`);

        let same = true;
        if (playerCards.length != status.playerCards.length) {
            same = false;
        } else {
            for (let i = 0; i < playerCards; ++i) {
                if (playerCards[i] != status.playerCards[i]) {
                    same = false;
                }
            }
        }
        if (!same) {
            playerCards = status.playerCards;
            cards = $('#player .hand-cards');
            cards.html("");
            for (let value of status.playerCards) {
                let domObject = $(`<div class="card"><img height="75px" src="assets/card_gold_${value}.svg" /></div>`);
                domObject.click({domObject, value}, selectCard);
                cards.append(domObject);
            }
        }
    }

    function selectCard(event) {
        if (!active) return;
        let card = event.data;
        if (selectedCard && selectedCard.value === card.value) {
            selectedCard.domObject.removeClass('selected');
            selectedCard = null;
            return;
        } 
        if (selectedCard) {
            selectedCard.domObject.removeClass('selected');
        } 
        card.domObject.addClass('selected');
        selectedCard = card;
    }

    function playCard(event) {
        if (!active) return;
        if (!selectedCard) return;
        $.get('/api/v1/playCard', { playerId, card: selectedCard.value, side: event.data.side, pile: event.data.pile })
            .done(() => {
                $.get('/api/v1/getStatus', { playerId }, function(response) {
                    updateStatus(response);
                });
        });
        selectedCard.removeClass('selected');
        selectedCard = null;
    }

    function drawCards(event) {
        if (!active) return;
        $.get('/api/v1/drawCards', { playerId })
            .done(() => {
                $.get('/api/v1/getStatus', { playerId }, function(response) {
                    updateStatus(response);
                });
        });
    }

    $(function() {
        playerId = window.location.search.split('?').pop().split('=').pop();
        $('#opponent .piles .pile-up').click({side: 'opponent', pile: 'up'}, playCard);
        $('#opponent .piles .pile-down').click({side: 'opponent', pile: 'down'}, playCard);
        $('#player .piles .pile-up').click({side: 'player', pile: 'up'}, playCard);
        $('#player .piles .pile-down').click({side: 'player', pile: 'down'}, playCard);
        $('#player .draw-pile').click(drawCards);
        $.get('/api/v1/getStatus', { playerId }, function(response) {
            updateStatus(response);
        });
        setInterval(() => {
            $.get('/api/v1/getStatus', { playerId }, function(response) {
                updateStatus(response);
            });
        }, 2000);
    });
}())