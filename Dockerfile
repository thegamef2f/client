FROM nginx:1.19.1

COPY ./nginx/conf.d /etc/nginx/conf.d
COPY ./src /usr/share/nginx/html
